<style>
    .main-panel {
        
         width: 100% !important;
         float: none !important; 
        
    }
    .main-header {
        display: none !important;
    }
    .sidebar, .sidebar[data-background-color=white] {
        display: none !important;
    }
    .cabecera-img{
        display:block;
        margin:auto;
      }
    
      .py-4 {
        padding-top: 3.5rem !important;
    }
    
    #titulo{
     
        margin-top: 5px;
        font-weight: 600;
        font-size: 14px;
        text-align: center;
        color: #676a6c;
    }
    
    .colorut{
        background-color: #20283f !important;
        border-color:  #20283f !important;
        cursor: pointer;
        color: #fff;
    }
    .m-t{
        margin-top: 1.5rem !important;
        color: #676a6c;
        text-align: center !important;
    }
    
    .footer {
        display: none !important;
    }
    #text-decoration-none{
  text-align: center ;
}
#alineado{
    text-align: center ;
    margin-top: 1rem;
}
    
    </style>

@extends('layouts.app')
@section('Titulo', 'Repositorio de investigaciones ')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div >
                <div ><img class="cabecera-img" src="{{asset('images/Logo.png') }} " width="310px" height="120px"></div>
                <h3 id="titulo"> Repositorio de investigaciones y publicaciones de Cuerpos Académicos </h3>
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Division') }}</label>

                            <div class="col-md-6">
                                <input id="division" type="text" class="form-control @error('name') is-invalid @enderror" name="division" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
          
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Curp') }}</label>

                            <div class="col-md-6">
                                <input id="Curp" type="text" class="form-control @error('name') is-invalid @enderror" name="curp" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('RFC') }}</label>

                            <div class="col-md-6">
                                <input id="RFC" type="text" class="form-control @error('name') is-invalid @enderror" name="rfc" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Categoria de profesor') }}</label>
                            <div class="col-md-6">
                                <select class="custom-select" id="inputGroupSelect01" name="cat_profesor">
                                <option value="PTC">PTC</option>
                                <option value="Coordinador de CAA">Coordinador de CAA</option>
                                <option value="Coordinador de División">Coordinador de División</option>
                                <option value="Jefe de departamento de Investigación">Jefe del departamento de Investigación</option>
                                <option value="Director de División">Director de División</option>
                                </select>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
          
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn  btn-lg btn-block colorut">
                                    {{ __('Register') }}
                                </button>
                                <p id="alineado"><a href="#" id="text-decoration-none">¿Ya tengo cuenta?</a> </p>
                            </div>
                        </div>
                    </form>
                    <p class="m-t">
                        <small>Universidad Tecnológica de Cancún</small>
                        <br>
                        <small>Organismo Público Descentralizado del Gobierno del Estado de Quintana Roo</small>
                        <br>
                        <small>Carretera Cancún-Aeropuerto, Km. 11.5, S.M. 299, Mz. 5, Lt 1</small>
                        <br>
                        <small>Cancún, Quintana Roo, C.P. 77565</small>
                        <br>
                        <small>Tel. 01 (998) 881 19 00</small>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
