@extends('layouts.app')

@section('Titulo', 'Repositorio de investigaciones ')

@section('content')

<!--  Este es el editar perfil -->

<div class="panel-header colorut" >
		<div class="page-inner py-5">
			<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
			    <div>
			        <h2 class="text-black pb-2 fw-bold">Tu perfil</h2>
		        </div>		
	        </div>
		</div>
    </div>
	<div class="page-inner mt--5">		
			
	    <!-- Contenido de el editar perfil -->

<!-- Tabla para editar el perfil -->
<div>
<table class="table table-hover">
	<thead>
		<tr>
			<th scope="col">Nombre</th>
			<th scope="col">Correo electrónico</th>
			<th scope="col">Division</th>
			<th scope="col">Curp</th>
            <th scope="col">RFC</th>
            <th scope="col">Categoria de profesor</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>1</td>
			<td>Mark</td>
			<td>Otto</td>
			<td>@mdo</td>
			<td>asdsadada</td>
			<td>sadadads</td>
		</tr>
		<tr>
			<td>2</td>
			<td>Jacob</td>
			<td>Thornton</td>
			<td>@fat</td>
			<td>safasfasf</td>
			<td>asfasfa</td>
		</tr>
		<tr>
			<td>3</td>
			<td >Larry the Bird</td>
			<td>@twitter</td>
			<td>asfasfafssaf</td>
			<td>safsfa</td>
			<td>asfasf</td>
	      
		</tr>
	</tbody>
</table>


<div>

<!-- Fin de tabla -->



@endsection